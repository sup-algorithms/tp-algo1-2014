package s03;
public class AmStramGram {
  public static void main(String[] args) {
    int n=8, k=3;
    if (args.length == 2) {
      n = Integer.parseInt(args[0]);
      k = Integer.parseInt(args[1]);
    }
    System.out.println("Winner is " + winnerAmStramGram(n, k));
  }
  // ----------------------------------------------------------  
  public static int winnerAmStramGram(int n, int k) {
    List     l = new List();
    ListItr li = new ListItr(l);
    int      i;

    // TODO - to complete !
    ...                                 // build '-/-1-2-3-4-...-n-'
    while(...) {                        // while size>1
      ...                               //   advance k times
      ...                               //   then remove an element
    }
    return ...                          // return the last one
  }
  // ----------------------------------------------------------  
}
