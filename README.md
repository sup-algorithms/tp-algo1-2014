Assignments files for lecture "Algorithms and Data-structures" 2014
===================================================================

You can use this repository as "upstream" to your workspace to simplify the setup for each assignment.

Check the wiki page for instruction on how to use this repository.

-- Jacques Supcik
