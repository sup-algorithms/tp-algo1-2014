package s16;
import java.util.Stack;
import java.util.Random;

public class ExoBTree {
  // ------------------------------------------------------------
  public static void recDepthFirst(BTNode t) {
    if (t==null) return;
    System.out.print(" "+t.elt);
    recDepthFirst(t.left );
    recDepthFirst(t.right);
  }
  // ------------------------------------------------------------
  public static void depthFirst (BTNode t) {
    Stack s = new Stack();
    BTNode crt;
    s.push(t);
    while(! s.isEmpty()) {
      crt = (BTNode) (s.pop()); 
      if (crt==null) continue;
      System.out.print(" "+crt.elt); 
      s.push(crt.right);
      s.push(crt.left );
    }
  }
  // ------------------------------------------------------------
  public static String breadthFirst (BTNode t) {
    String res="";
    BTNode crt;
    Queue q = new Queue();
    q.enqueue(t);
    while(! q.isEmpty()) {
      crt = (BTNode) (q.dequeue()); 
      if (crt==null) continue;
      res += (" "+crt.elt); 
      q.enqueue(crt.left );
      q.enqueue(crt.right);
    }
    return res;
  }
  // ------------------------------------------------------------
  public static int size(BTNode t) {
    // TODO - to complete...
  }
  // ------------------------------------------------------------
  public static int height(BTNode t) {
    // TODO - to complete...
  }
  // ------------------------------------------------------------
  public static String breadthFirst1 (BTNode t) {
    // TODO - to complete...
  }
  // ------------------------------------------------------------
  public static String visitLevel(BTNode t, int level) {
    // TODO - to complete...
  }

  // ------------------------------------------------------------
  public static void rotateRight(BTNode y) {
    // TODO - to complete...
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  private static Random rnd = new Random();
  // ------------------------------------------------------------
  public static BTNode rndTree(int size) {
    int d=0;
    if (size==0) return null;
    BTNode root = new BTNode(new Integer(0), null, null, null);
    BTNode t=root;
    boolean isLeft;
    for(int i=1; i<size; i++) {
      t=root;
      while(true) {
        isLeft=rnd.nextBoolean();
        if (isLeft)
          if (t.left ==null) break; else t=t.left;
        else
          if (t.right==null) break; else t=t.right;
      }
        BTNode newLeaf = new BTNode(new Integer(i), null, null, t);
        if (isLeft) t.left =newLeaf;
        else        t.right=newLeaf;
    }
    return root;
  }
  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nbOfNodes = 10;
    BTNode t = rndTree(nbOfNodes);
    System.out.println("Tree:" +t);
    System.out.println(t.toReadableString());
    //System.out.println("\nDepth first, preorder:");
    //depthFirst(t); 
    System.out.println("\nRecursive depth first, preorder:");
    recDepthFirst(t);
    System.out.println("\nBreadth first:");
    System.out.println(breadthFirst(t));
    System.out.println("\nBreadth first bis:");
    System.out.println(breadthFirst1(t));
    System.out.println("\nSize:" + size(t));
    System.out.println("\nHeight:" + height(t));
  }
  // ------------------------------------------------------------
}

