package s17;
import java.util.Random;
// ------------------------------------------------------------
public class IntIntPtyQueue {
  private IntQueueArray [] qt;
  // or: private IntQueueChained [] qt;
  // ...
  // ------------------------------------------------------------
  // priorities will be in 0..theMaxPty
  public IntIntPtyQueue(int theMaxPty) {
    // TODO - to complete
  }
  // ------------------------------------------------------------
  public boolean isEmpty() {
    // TODO - to complete
  }
  // ------------------------------------------------------------
  // PRE: 0<=pty<maxPty
  public void    enqueue(int elt, int pty) {
    // TODO - to complete
  }
  // ------------------------------------------------------------
  //highest pty present in the queue.
  //PRE: ! isEmpty()
  public int     consultPty() {
    // TODO - to complete
  }
  // ------------------------------------------------------------
  // elt with highest (smallest) pty.  
  // PRE: ! isEmpty()
  public int     consult() {
    // TODO - to complete
  }
  // ------------------------------------------------------------
  // elt with highest (smallest) pty.  
  // PRE: ! isEmpty()
  public int     dequeue() {
    // TODO - to complete
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String [] args) {
    Random r = new Random();
    long seed = r.nextInt(1000);
    r.setSeed(seed);
    System.out.println("Using seed "+seed);
    int n = 10000;
    if (args.length == 1) 
      n = Integer.parseInt(args[0]);
    int p, e;
    IntIntPtyQueue pq = new IntIntPtyQueue(n);
    for(int i=0; i<10*n; i++) {
      p = r.nextInt(n); pq.enqueue(p, p);
    }
    e = Integer.MIN_VALUE;
    for (int i=0; i<10*n; i++) {
      p = pq.dequeue(); ok (p>=e); e=p;
    }
    ok(pq.isEmpty());
    for(int i=0; i<10*n; i++) {
      p = r.nextInt(n); pq.enqueue(p, p);
      p = r.nextInt(n); pq.enqueue(p, p);
      pq.dequeue();
    }
    e = Integer.MIN_VALUE;
    while(!pq.isEmpty()) {
      p = pq.dequeue(); ok (p>=e); e=p;
    }
    System.out.println("Test passed successfully");
  }
  // ------------------------------------------------------------
  static void ok(boolean b) {
    if (b) return;
    throw new RuntimeException("property not verified: ");
  }
}

